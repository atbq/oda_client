(function () {
    'use strict';

    $.Oda.Controller['404'] = {
        /**
         * @returns {$.Oda.Controller.404}
         */
        start: function () {
            try {
                return this;
            } catch (er) {
                $.Oda.Log.error("$.Oda.Controller['404'].start: " + er.message);
                return null;
            }
        }
    };

}());