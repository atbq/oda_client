(function() {
    'use strict';

    $.Oda.Controller.Admin = {
        /* $.Oda.Controller.Admin.currentUser */
        currentUser: null,
        /* $.Oda.Controller.Admin.ranks */
        ranks: null,
        /**
         * @name $.Oda.Controller.Admin.start
         * @returns {$.Oda.Controller.Admin}
         */
        start: function() {
            try {
                $.Oda.Interface.callRest($.Oda.Context.rest + 'vendor/atbq/oda/resources/api/rank/', { 
                    callback: function(response) {
                        $.Oda.Controller.Admin.ranks = response.data;
                    }
                });
                $.Oda.Controller.Admin.displayListUsers();
                $.Oda.Controller.Admin.maillingListDisplay();
                $.Oda.Controller.Admin.messagesDisplay();
                return this;
            } catch (er) {
                $.Oda.Log.error('$.Oda.Controller.Admin.start: ' + er.message);
                return null;
            }
        },
        /**
         * @name $.Oda.Controller.Admin.displayListUsers
         * @returns {$.Oda.Controller.Admin}
         */
        displayListUsers: function() {
            try {
                $.Oda.Interface.callRest($.Oda.Context.rest + 'vendor/atbq/oda/resources/api/user/', { 
                    callback: function(response) {
                        var objDataTable = $.Oda.Tooling.objDataTableFromJsonArray(response.data);
                        
                        $.Oda.Display.TemplateHtml.create({
                            template: 'user-tab-tpl',
                            callback: function(strHtml){
                                $('#div_listUtilisateurs').html(strHtml);
                            }
                        });

                        var oTable = $('#table_listUtilisateurs').DataTable({
                            sPaginationType: 'full_numbers',
                            aaData: objDataTable.data,
                            aaSorting: [[4, 'desc'], [1, 'asc']],
                            aoColumns: [
                                {
                                    sTitle: '<oda-label oda-label-value="oda-admin.userCode"/>', 
                                    sClass: 'dataTableColCenter'
                                },
                                {
                                    sTitle: '<oda-label oda-label-value="oda-admin.user"/>'
                                },
                                {
                                    sTitle: '<oda-label oda-label-value="oda-admin.mail"/>'
                                },
                                {
                                    sTitle: '<oda-label oda-label-value="oda-admin.desc"/>'
                                },
                                {
                                    sTitle: '<oda-label oda-label-value="oda-admin.active"/>', 
                                    sClass: "dataTableColCenter"
                                },
                                {
                                    sTitle: '<oda-label oda-label-value="oda-admin.rank"/>'
                                },
                                {
                                    sTitle: '<oda-label oda-label-value="oda-admin.actions"/>', 
                                    sClass: 'dataTableColCenter'
                                }
                            ],
                            aoColumnDefs: [
                                {
                                    mRender: function (data, type, row) {
                                        return row[objDataTable.entete['code_user']];
                                    },
                                    aTargets: [0]
                                },
                                {
                                    mRender: function (data, type, row) {
                                        return row[objDataTable.entete['name_first']] + ', ' + row[objDataTable.entete['name_last']];
                                    },
                                    aTargets: [1]
                                },
                                {
                                    mRender: function (data, type, row) {
                                        return row[objDataTable.entete['mail']];
                                    },
                                    aTargets: [2]
                                },
                                {
                                    mRender: function (data, type, row) {
                                        return row[objDataTable.entete['description']];
                                    },
                                    aTargets: [3]
                                },
                                {
                                    mRender: function (data, type, row) {
                                        return row[objDataTable.entete['active']];
                                    },
                                    aTargets: [4]
                                },
                                {
                                    mRender: function (data, type, row) {
                                        return $.Oda.I8n.getByString(row[objDataTable.entete['rank_label']]);
                                    },
                                    aTargets: [5]
                                },
                                {
                                    mRender: function (data, type, row) {
                                        var strHtml = '<a onclick="$.Oda.Controller.Admin.userEdit(\'' + row[objDataTable.entete['code_user']] + '\')" id="bt_edit_' + row[objDataTable.entete['code_user']] + '" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> ' + $.Oda.I8n.get('oda-main', 'bt-edit') + '</a>';
                                        return strHtml;
                                    },
                                    aTargets: [6]
                                }
                            ]
                        });

                        $('#table_listUtilisateurs tfoot th').each(function (i) {
                            var valOdaAttri = $(this).attr('oda-attr');
                            if (valOdaAttri === 'text') {
                                $('<input type="text" placeholder="Search" size="4"/>')
                                    .appendTo($(this).empty())
                                    .on('keyup change', function() {
                                        oTable
                                            .column(i)
                                            .search(this.value)
                                            .draw();
                                    });
                            }
                        });
                    }
                });

                return this;
            } catch (er) {
                $.Oda.Log.error('$.Oda.Controller.Admin.displayListUsers: ' + er.message);
                return null;
            }
        },
        /**
         * @name $.Oda.Controller.Admin.userEdit
         * @param {string} userCode
         * @returns {$.Oda.Controller.Admin}
         */
        userEdit: function (userCode) {
            try {
                $.Oda.Controller.Admin.currentUser = userCode;
                $.Oda.Interface.callRest($.Oda.Context.rest + 'vendor/atbq/oda/resources/api/user/' + $.Oda.Controller.Admin.currentUser, { 
                    callback: function(response) {

                        var label = $.Oda.I8n.get('oda-admin', 'edit-label', {
                            variables: {
                                userCode: response.data.code_user
                            }
                        });
                        
                        var strRanks = $.Oda.Tooling.Json.toDOM({
                            json: $.Oda.Controller.Admin.ranks
                        });
                        var strHtml = $.Oda.Display.TemplateHtml.create({
                            template: 'edit-template',
                            scope: {
                                edit_mail: response.data.mail,
                                edit_desc: response.data.description,
                                edit_actif_selected: response.data.actif,
                                ranks: strRanks,
                                selectedRank: response.data.id_rang
                            }
                        });

                        var strHtmlFooter = $.Oda.Display.TemplateHtml.create({
                            template: 'edit-footer-tpl'
                        });

                        $.Oda.Display.Popup.open({
                            label: label,
                            details: strHtml,
                            footer: strHtmlFooter,
                            callback: function(){
                                $.Oda.Scope.Gardian.add({
                                    id: 'gardianEdit',
                                    listElt: ['edit_mail', 'edit_desc','edit_actif', 'input_rang'],
                                    function: function(params){
                                        if(($('#edit_mail').data('isOk')) && ($('#edit_desc').data('isOk')) && ($('#edit_actif').data('isOk')) && ($('#input_rang').data('isOk'))){
                                            $('#bt_validerEdit').btEnable();
                                        }else{
                                            $('#bt_validerEdit').btDisable();
                                        }
                                    }
                                });
                            }
                        });
                    }
                });
                return this;
            } catch (er) {
                $.Oda.Log.error('$.Oda.Controller.Admin.userEdit: ' + er.message);
                return null;
            }
        },
        /**
         * @name $.Oda.Controller.Admin.userEditionValidate
         * @returns {$.Oda.Controller.Admin}
         */
        userEditionValidate: function() {
            try {
                var mail = $('#edit_mail').val();
                var desc = $('#edit_desc').val();
                var active = $('#edit_actif').val();
                var rankId = $('#input_rang').val();

                $.Oda.Interface.callRest($.Oda.Context.rest + 'vendor/atbq/oda/resources/api/user/' + $.Oda.Controller.Admin.currentUser, {
                    type: 'put', 
                    callback:function(){
                        $.Oda.Controller.Admin.currentUser = null;
                        $.Oda.Display.Popup.close();
                        $.Oda.Display.Notification.infoI8n('oda-admin.userUpdateOk');
                        $.Oda.Controller.Admin.displayListUsers();
                    }
                }, { 
                    mail: mail,
                    active: active,
                    rankId: rankId,
                    desc: desc
                });
                return this;
            } catch (er) {
                $.Oda.Log.error('$.Oda.Controller.Admin.userEditionValidate: ' + er.message);
                return null;
            }
        },
        /**
         * @name $.Oda.Controller.Admin.maillingListDisplay
         * @desc Affiche la liste de diffusion pour le site
         * @returns {$.Oda.Controller.Admin}
         */
        maillingListDisplay: function() {
            try {
                $.Oda.Interface.callRest($.Oda.Context.rest + 'vendor/atbq/oda/resources/api/user/mail/', { 
                    callback: function(response) {
                        var strhtml = '';
                        for (var index in response.data) {
                            strhtml += response.data[index].mail + ';';
                        }
                        $('#div_listDiffusion').html(strhtml);
                    }
                });
                return this;
            } catch (er) {
                $.Oda.Log.error('$.Oda.Controller.Admin.maillingListDisplay: ' + er.message);
                return null;
            }
        },
        /**
         * @name $.Oda.Controller.Admin.messagesDisplay
         * @returns {$.Oda.Controller.Admin}
         */
        messagesDisplay: function() {
            try {
                var $divMessages = $('#div_messages');
                $divMessages.html('<oda-loading/>')
                $.Oda.Interface.callRest($.Oda.Context.rest+"vendor/atbq/oda/resources/api/message/", {
                    callback: function(response){
                        $.Oda.Display.TemplateHtml.create({
                            template: 'message-tpl',
                            callback: function(strHtml){
                                $divMessages.html(strHtml);

                                $.Oda.Display.Table.createDataTable({
                                    target: 'table_messages',
                                    data: response.data,
                                    attribute: [
                                        {
                                            header: 'oda-admin.id',
                                            align: 'center',
                                            value: function(data, type, full, meta, row){
                                                return row.id;
                                            }
                                        },
                                        {
                                            header: 'oda-admin.message',
                                            value: function(data, type, full, meta, row){
                                                return row.message.substring(0, 25);
                                            }
                                        },
                                        {
                                            header: 'oda-admin.rank',
                                            value: function(data, type, full, meta, row){
                                                return $.Oda.I8n.getByString(row.rank_label);
                                            }
                                        },
                                        {
                                            header: 'oda-admin.level',
                                            value: function(data, type, full, meta, row){
                                                return row.level;
                                            }
                                        },
                                        {
                                            header: 'oda-admin.expiration',
                                            value: function(data, type, full, meta, row){
                                                if(type === 'display') {
                                                    return $.Oda.Date.dateFormat(row.expiration, $.Oda.I8n.get('oda-main', 'dateTimeFormat'));
                                                }
                                                return row.expiration;
                                            }
                                        },
                                        {
                                            header: 'oda-admin.creator',
                                            value: function(data, type, full, meta, row){
                                                return row.user_code;
                                            }
                                        },
                                        {
                                            header: 'oda-admin.creation',
                                            value: function(data, type, full, meta, row){
                                                if(type === 'display') {
                                                    return $.Oda.Date.dateFormat(row.creation, $.Oda.I8n.get('oda-main', 'dateTimeFormat'));
                                                }
                                                return row.creation;
                                            }
                                        }
                                    ]
                                });
                            }
                        });
                    }
                });
                return this;
            } catch (er) {
                $.Oda.Log.error('$.Oda.Controller.Admin.messagesDisplay: ' + er.message);
                return null;
            }
        },
        /**
         * @name $.Oda.Controller.Admin.messageNew
         * @returns {$.Oda.Controller.Admin}
         */
        messageNew: function() {
            try {
                var strRanks = $.Oda.Tooling.Json.toDOM({
                    json: $.Oda.Controller.Admin.ranks
                });
                var strHtmlDetails = $.Oda.Display.TemplateHtml.create({
                    template: 'tplNewMessage',
                    scope: {
                        ranks: strRanks
                    }
                });

                var strHtmlFooter = $.Oda.Display.TemplateHtml.create({
                    template: 'newMessage-footer-tpl'
                });

                $.Oda.Display.Popup.open({
                    label: $.Oda.I8n.get('oda-admin', 'bt-create-new-message'),
                    details: strHtmlDetails,
                    footer: strHtmlFooter,
                    callback: function(){
                        $.Oda.Scope.Gardian.add({
                            id: 'gardianNewMsg',
                            listElt: ['input_message', 'input_niveau', 'input_profile'],
                            function: function(){
                                if(($('#input_message').data('isOk')) && ($('#input_niveau').data('isOk')) && ($('#input_profile').data('isOk'))){
                                    $('#bt_validerCreerMsg').btEnable();
                                }else{
                                    $('#bt_validerCreerMsg').btDisable();
                                }
                            }
                        });
                    }
                });
                return this;
            } catch (er) {
                $.Oda.Log.error('$.Oda.Controller.Admin.messageNew: ' + er.message);
                return null;
            }
        },
        /**
         * @name $.Oda.Controller.Admin.messageNewSubmit
         * @returns {$.Oda.Controller.Admin}
         */
        messageNewSubmit: function() {
            try {
                var input_message = CKEDITOR.instances['input_message'].getData();
                var input_niveau = $('#input_niveau').val();
                var input_date_expiration = $('#input_date_expiration').val();
                var input_profile = $('#input_profile').val();

                $.Oda.Interface.callRest($.Oda.Context.rest + "vendor/atbq/oda/resources/api/message/", {
                    type: 'post', 
                    callback: function(response){
                        $('#input_message').val('');
                        $('#input_niveau').val('');
                        $('#input_date_expiration').val('');
                        $('#input_profile').val('');
                        $.Oda.Display.Popup.close();
                        $.Oda.Controller.Admin.messagesDisplay();
                        $.Oda.Display.Notification.infoI8n('oda-admin.messageCreateOk');
                    }
                }, {
                    userId: $.Oda.Session.id,
                    message: input_message,
                    level: input_niveau,
                    expirationDate: input_date_expiration,
                    rankId: input_profile
                });

                return this;
            } catch (er) {
                $.Oda.Log.error('$.Oda.Controller.Admin.messageNewSubmit: ' + er.message);
                return null;
            }
        },
    };

}());