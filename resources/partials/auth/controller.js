(function () {
    'use strict';

    $.Oda.Controller.Auth = {
        /**
         * @name $.Oda.Controller.Auth.start
         * @returns {$.Oda.Controller.Auth}
         */
        start: function () {
            try {
                $.Oda.Scope.Gardian.add({
                    id: 'gardianAuth',
                    listElt: ['login', 'password'],
                    function: function(e){
                        if( ($('#login').data('isOk')) && ($('#password').data('isOk')) ){
                            $('#submit').btEnable();
                        }else{
                            $('#submit').btDisable();
                        }
                    }
                });

                if($.Oda.Google.apiKey !== null && $.Oda.Google.clientId !== null){
                    $.Oda.Google.startSessionAuth(
                        function(){
                            $('#google').html('<oda-btn oda-btn-style="danger" oda-btn-click="$.Oda.Controller.Auth.goInWithGoogle();">oda-auth.bt-google</oda-btn>');
                        }
                        , function(){
                            $('#google').html('<oda-btn oda-btn-style="danger" oda-btn-click="$.Oda.Google.callServiceGoogleAuth($.Oda.Controller.Auth.goInWithGoogle, $.Oda.App.Controller.Auth.callServiceGoogleAuthKo);">oda-auth.bt-google</oda-btn>');
                        }
                    );
                }
                return this;
            } catch (er) {
                $.Oda.Log.error('$.Oda.Controller.Auth.start: ' + er.message);
                return null;
            }
        },
        /**
         * @name $.Oda.Controller.Auth.goIn
         * @returns {$.Oda.Controller.Auth}
         */
        goIn: function () {
            try {
                $.Oda.Security.auth({
                    login: $('#login').val(),
                    mdp: $('#password').val(), 
                    reload: true
                });
                return this;
            } catch (er) {
                $.Oda.Log.error('$.Oda.Controller.Auth.goIn: ' + er.message);
                return null;
            }
        },
        /**
         * @name $.Oda.Controller.Auth.goInWithGoogle
         * @returns {$.Oda.Controller.Auth}
         */
        goInWithGoogle: function () {
            try {
                gapi.client.oauth2.userinfo.get().execute(function(resp) {
                    $.Oda.Interface.callRest($.Oda.Context.rest+"vendor/atbq/oda/resources/api/user/search/mail/", {callback:function(retour){
                        if(retour.data.lenght === 0){
                            $.Oda.Display.Notification.warning($.Oda.I8n.get('oda-auth', 'noAccount') + resp.email);
                        }else if(retour.data.lenght === 1){
                            $.Oda.Security.auth({login: retour.data[0].code_user, mdp: 'authByGoogle-' + resp.email, reload: true});
                        }else {
                            var strHtml = '<div class="list-group">';
                            for (var indice in retour.data) {
                                var elt = retour.data[indice];
                                strHtml += "<a class=\"list-group-item\" onclick=\"$.Oda.Security.auth({login: '" + elt.code_user + "', mdp: '" + "authByGoogle-" + resp.email + "', reload: true});\">" + elt.code_user + "</a>";
                            }
                            strHtml += '</div>';
                            $.Oda.Display.Popup.open({label: $.Oda.I8n.get('oda-auth', 'chooseAccount'), details: strHtml});
                        }
                    }}, { 
                        email: resp.email
                    });
                });
                return this;
            } catch (er) {
                $.Oda.Log.error('$.Oda.Controller.Auth.goInWithGoogle: ' + er.message);
                return null;
            }
        },
        /**
         * @name $.Oda.Controller.Auth.callServiceGoogleAuthKo
         * @returns {$.Oda.Controller.Auth}
         */
        callServiceGoogleAuthKo: function () {
            try {
                $.Oda.Display.Notification.i8nWarning("oda-auth.bt-google-fail");
                return this;
            } catch (er) {
                $.Oda.Log.error("$.Oda.App.Controller.Auth.callServiceGoogleAuthKo: " + er.message);
                return null;
            }
        }
    };

}());