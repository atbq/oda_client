(function () {
    'use strict';

    $.Oda.Controller.Profile = {
        /**
         * @name $.Oda.Controller.Profile.start
         * @returns {$.Oda.Controller.Profile}
         */
        start: function () {
            try {
                $('[oda-avatar-name=avatarProfile]').attr('oda-avatar-user',$.Oda.Session.code_user);
                $.Oda.Controller.Profile.loadingInfos();
                return this;
            } catch (er) {
                $.Oda.Log.error('$.Oda.Controller.Profile.start: ' + er.message);
                return null;
            }
        },

        /**
         * @name $.Oda.Controller.Profile.setAvatar
         * @param {Object} p
         * @param p.elt
         * @returns {$.Oda.Controller.Profile.setAvatar}
         */
        setAvatar: function (p) {
            try {
                $('[oda-avatar-name=avatar]').attr('oda-avatar-user', '');
                $('[oda-avatar-name=avatarProfile]').attr('oda-avatar-user', '');
                $.Oda.Tooling.postResources({
                    idInput: p.elt.id, 
                    folder: 'avatars/', 
                    names: [$.Oda.Session.code_user], 
                    callback: function(response){
                        var response = response[$.Oda.Session.code_user];
                        if(response.status === 'TRANS_STATUS_SUCCESS'){
                            $.Oda.Display.Notification.success($.Oda.I8n.get('oda-profile', 'uploadSuccess'));
                        }else{
                            $.Oda.Display.Notification.error($.Oda.I8n.get('oda-profile', 'uploadError', {
                                variables: {
                                    msg: response.msg
                                }
                            }));
                        }
                        $('[oda-avatar-name=avatar]').attr('oda-avatar-user', $.Oda.Session.code_user);
                        $('[oda-avatar-name=avatarProfile]').attr('oda-avatar-user', $.Oda.Session.code_user);
                    }
                });
                return this;
            } catch (er) {
                $.Oda.Log.error('$.Oda.Controller.Profile.setAvatar: ' + er.message);
                return null;
            }
        },

        /**
         * @name $.Oda.Controller.Profile.loadingInfos
         * @returns {$.Oda.Controller.Profile.setAvatar}
         */
        loadingInfos: function(){
            try {
                $.Oda.Interface.callRest($.Oda.Context.rest+'vendor/atbq/oda/resources/api/user/current', { callback: function(response){
                    var mail = response.data.mail;
                    var code_user = response.data.code_user;
                    var name_first = response.data.name_first;
                    var name_last = response.data.name_last;
                    var language = response.data.language;
                    var rankLabel = response.data.rank_label;

                    $('#code_user').html(code_user);
                    $('#firstName').html(name_first);
                    $('#lastName').html(name_last);
                    $('#mail').html(mail);
                    $('#language').html(language);
                    $('#rank').html("<oda-label oda-label-value='" + rankLabel + "'/>");
                }}, { 
                    code_user: $.Oda.Session.code_user,
                    profile: $.Oda.Session.userInfo.profile
                });
                return this;
            } catch (er) {
                $.Oda.Log.error('$.Oda.Controller.Profile.loadingInfos: ' + er.message);
                return null;
            }
        },

        /**
         * @name $.Oda.Controller.Profile.updateChamps
         * @param {String} p
         */
        updateChamps: function(p){
            try {
                var label = '';
                var details = '';
                var footer = '';

                switch (p){
                    case 'firstName':
                        label = $.Oda.I8n.get('oda-profile', 'update') + $.Oda.I8n.get('oda-profile', 'firstName');
                        details += '<oda-input-text oda-input-text-name="input_firstName" oda-input-text-type="text" oda-input-text-label="oda-main.subscrib-firstName" oda-input-text-tips="oda-main.subscrib-firstName-tips" oda-input-text-placeholder="oda-main.subscrib-firstName-placeholder" oda-input-text-check="Oda.Regexs:firstName" oda-input-text-value="' + $.Oda.Session.userInfo.firstName + '" required/>';
                        footer += '<oda-btn oda-btn-name="submit" oda-btn-enter="1" oda-btn-style="info" oda-btn-click="$.Oda.Controller.Profile.submitChange(\'firstName\');" disabled>oda-main.bt-submit</oda-btn>';
                        break;
                    case 'lastName':
                        label = $.Oda.I8n.get('oda-profile', 'update') + $.Oda.I8n.get('oda-profile', 'lastName');
                        details += '<oda-input-text oda-input-text-name="input_lastName" oda-input-text-type="text" oda-input-text-label="oda-main.subscrib-lastName" oda-input-text-tips="oda-main.subscrib-lastName-tips" oda-input-text-placeholder="oda-main.subscrib-lastName-placeholder" oda-input-text-check="Oda.Regexs:lastName" oda-input-text-value="' + $.Oda.Session.userInfo.lastName + '" required/>';
                        footer += '<oda-btn oda-btn-name="submit" oda-btn-enter="1" oda-btn-style="info" oda-btn-click="$.Oda.Controller.Profile.submitChange(\'lastName\');" disabled>oda-main.bt-submit</oda-btn>';
                        break;
                    case 'mail':
                        label = $.Oda.I8n.get('oda-profile', 'update') + $.Oda.I8n.get('oda-profile', 'mail');
                        details += '<oda-input-text oda-input-text-name="input_mail" oda-input-text-type="email" oda-input-text-label="oda-main.contact-mail" oda-input-text-tips="oda-main.contact-mail-tips" oda-input-text-placeholder="oda-main.contact-mail-placeholder" oda-input-text-check="Oda.Regexs:mail" oda-input-text-value="' + $.Oda.Session.userInfo.mail + '" required/>';
                        footer += '<oda-btn oda-btn-name="submit" oda-btn-enter="1" oda-btn-style="info" oda-btn-click="$.Oda.Controller.Profile.submitChange(\'mail\');" disabled>oda-main.bt-submit</oda-btn>';
                        break;
                    case 'language':
                        label = $.Oda.I8n.get('oda-profile', 'update') + $.Oda.I8n.get('oda-profile', 'language');
                        details += '<oda-input-select oda-input-select-name="input_language" oda-input-select-label="oda-profile.language" oda-input-select-value="' + $.Oda.Session.userInfo.locale + '" oda-input-select-availables="[{id:\'fr\',\'label\':\'oda-profile.fr\'},{id:\'en\',\'label\':\'oda-profile.en\'},{id:\'es\',\'label\':\'oda-profile.es\'}]" oda-input-select-display="elt.label" oda-input-select-response="elt.id" oda-input-select-order="asc" required/>';
                        footer += '<oda-btn oda-btn-name="submit" oda-btn-enter="1" oda-btn-style="info" oda-btn-click="$.Oda.Controller.Profile.submitChange(\'language\');" disabled>oda-main.bt-submit</oda-btn>';
                        break;
                }

                var content = $.Oda.Display.TemplateHtml.create({
                    template: 'tplUpdateField',
                    scope: {
                        content: details
                    }
                });

                $.Oda.Display.Popup.open({
                    name: 'popChangeProfile',
                    label: label,
                    details: content,
                    footer: footer,
                    callback: function(){
                        $.Oda.Scope.Gardian.add({
                            id: 'gardianChangeProfile',
                            listElt: ['input_old_password', 'input_firstName', 'input_lastName', 'input_mail'],
                            function: function(e){
                                if( ($('#input_old_password').data('isOk'))
                                    && ( !$('#input_firstName').exists() || $('#input_firstName').data('isOk'))
                                    && ( !$('#input_lastName').exists() || $('#input_lastName').data('isOk'))
                                    && ( !$('#input_mail').exists() || $('#input_mail').data('isOk'))
                                ){
                                    $('#submit').btEnable();
                                }else{
                                    $('#submit').btDisable();
                                }
                            }
                        });
                    }
                });
            } catch (er) {
                $.Oda.Log.error('$.Oda.Controller.Profile.updateChamps: ' + er.message);
            }
        },

        /**
         * @name $.Oda.Controller.Profile.submitChange
         * @param {string} p
         */
        submitChange: function(p){
            try {
                var field;
                switch (p) {
                    case 'firstName':
                        field = 'name_first';
                        break;
                    case 'lastName':
                        field = 'name_last';
                        break;
                    case 'mail':
                        field = 'mail';
                        break;
                    case 'language':
                        field = 'language';
                        break;
                }
                var $field = $('#'+p);
                var $input = $('#input_'+p);
                $.Oda.Interface.callRest($.Oda.Context.rest+'vendor/atbq/oda/resources/api/user/current', {type: 'put', callback: function(response){
                    //UPDATE SESSION
                    switch (p) {
                        case 'nameFirst':
                            $.Oda.Session.userInfo.firstName = $input.val();
                            break;
                        case 'nameLast':
                            $.Oda.Session.userInfo.lastName = $input.val();
                            break;
                        case 'mail':
                            $.Oda.Session.userInfo.mail = $input.val();
                            break;
                        case 'language':
                            $.Oda.Session.userInfo.locale = $input.val();
                            $.Oda.I8n.watchLanguage();
                            break;
                    }
                    $.Oda.Storage.set("ODA-SESSION", $.Oda.Session, 43200);

                    //UPDATE PAGE DISPLAY
                    $field.html($input.val());
                    
                    //RESET MODAL
                    $.Oda.Display.Popup.close({name: 'popChangeProfile'});
                    $('#input_old_password').val('');
                    $input.val('');

                    //SAY OK
                    $.Oda.Display.Notification.success($.Oda.I8n.get('oda-profile', 'updateSuccess'));
                }}, {
                    password: $('#input_old_password').val(),
                    field: field,
                    value: $input.val()
                });
            } catch (er) {
                $.Oda.Log.error('$.Oda.Controller.Profile.submitChange: ' + er.message);
            }
        }
    };

}());