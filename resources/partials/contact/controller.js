(function () {
    'use strict';

    $.Oda.Controller.Contact = {
        /**
         * @returns {$.Oda.Controller.Contact}
         */
        start: function () {
            try {
                if($.Oda.Session.id !== 0){
                    $('#name').val($.Oda.Session.userInfo.firstName + " " + $.Oda.Session.userInfo.lastName);
                    $('#mail').val($.Oda.Session.userInfo.mail);
                }
    
                $.Oda.Scope.Gardian.add({
                    id: "gardianContact",
                    listElt: ["name", "mail", "msg"],
                    function: function(params){
                        if( ($("#name").data("isOk")) && ($("#mail").data("isOk")) && ($("#msg").data("isOk")) ){
                            $("#submit").btEnable();
                        }else{
                            $("#submit").btDisable();
                        }
                    }
                });
                return this;
            } catch (er) {
                $.Oda.Log.error("$.Oda.Controller.Contact.start: " + er.message);
                return null;
            }
        },
        /**
         */
        contact: function () {
            try {
                $("#submit").btDisable();
                var contact_mail_administrateur = $.Oda.Interface.getParameter("contact_mail_administrateur");
                if (contact_mail_administrateur !== "") {
                    var message_html  = $.Oda.Display.TemplateHtml.create({
                        template: "mailContact",
                        scope: {
                            userCode: $.Oda.Session.code_user,
                            name: $('#name').val(),
                            mail: $('#mail').val(),
                            msg: $('#msg').val(),
                            siteUrl: $.Oda.Context.host
                        }
                    });
    
                    var sujet = "[ODA-" + $.Oda.Interface.getParameter("instance_name") + "]Nouveau contact.";
    
                    $.Oda.Interface.mail({
                        to: contact_mail_administrateur,
                        from: contact_mail_administrateur,
                        fromLabel: "Service Mail ODA",
                        reply: contact_mail_administrateur,
                        replyLabel: "Service Mail ODA",
                        cc: contact_mail_administrateur,
                        bodyHtml: message_html,
                        subject: sujet,
                        callback: function(response){
                            if(response.data){
                                $.Oda.Display.Notification.infoI8n("oda-contact.success");
                            }else{
                                $.Oda.Display.Notification.errorI8n("oda-contact.fail");
                            }
                            $("#mail").val("").change();
                            $("#name").val("").change();
                            $("#msg").val("").change();
                        }
                    });
                } else {
                    $.Oda.Display.Notification.errorI8n("oda-contact.ko");
                }
                $("#submit").btEnable();
            } catch (er) {
                $.Oda.Log.error("$.Oda.Controller.Contact: " + er.message);
            }
        }
    };
}());