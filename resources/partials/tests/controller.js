(function () {
    'use strict';

    $.Oda.Controller.Tests = {
        /**
         * @returns {$.Oda.Controller.Tests}
         */
        start: function () {
            try {
                $('#myTabs a').click(function (e) {
                    e.preventDefault()
                    $(this).tab('show')
                })

                $.Oda.Scope.Gardian.add({
                    id: "gardianMail",
                    listElt: ["mail"],
                    function: function(e){
                        if( ($("#mail").data("isOk")) ){
                            $("#submit").btEnable();
                        }else{
                            $("#submit").btDisable();
                        }
                    }
                });

                if($.Oda.Context.dev){
                    $("#alert-cache").show();
                }else{
                    $("#divBtTestCache").show();
                }

                return this;
            } catch (er) {
                $.Oda.Log.error("$.Oda.Controller.Tests.start: " + er.message);
                return null;
            }
        },
        /**
         * @returns {$.Oda.Controller.Tests}
         */
        sendMail: function () {
            try {
                $('#submit').btDisable();
                var contact_mail_administrateur = $.Oda.Interface.getParameter("contact_mail_administrateur");

                $.Oda.Display.TemplateHtml.create({
                    template: "mailTest",
                    scope: {
                        siteUrl: $.Oda.Context.host
                    },
                    callback: function(strHtml){
                        $.Oda.Interface.mail({
                            to: $('#mail').val(),
                            from: contact_mail_administrateur,
                            fromLabel: "Service Mail ODA",
                            reply: contact_mail_administrateur,
                            replyLabel: "Service Mail ODA",
                            cc: contact_mail_administrateur,
                            bodyHtml: strHtml,
                            subject: $.Oda.I8n.get("oda-tests","mailSubjet"),
                            callback: function(response){
                                if(response.data){
                                    $.Oda.Display.Notification.successI8n("oda-tests.mailSend");
                                }else{
                                    $.Oda.Display.Notification.error("fail");
                                }
                                $("#submit").btEnable();
                            }
                        });
                    }
                });

                return this;
            } catch (er) {
                $.Oda.Log.error("$.Oda.Controller.Tests.sendMail : " + er.message);
                return null;
            }
        },
        /**
         * @returns {$.Oda.Controller.Tests}
         */
        testCache: function () {
            try {
                $.Oda.Interface.callRest($.Oda.Context.rest+"vendor/atbq/oda/resources/api/sys/param/maintenance", {callback: function(response){
                    console.log(response);
                }});
                return this;
            } catch (er) {
                $.Oda.Log.error("$.Oda.Controller.Tests.testCache: " + er.message);
                return null;
            }
        },
        /**
         * @returns {$.Oda.Controller.Tests}
         */
        testMockup: function () {
            try {
                $.Oda.Interface.callRest($.Oda.Context.rest+"vendor/atbq/oda/resources/api/sys/test", {callback: function(response){
                    console.log(response);
                }});
                return this;
            } catch (er) {
                $.Oda.Log.error("$.Oda.Controller.Tests.testMockup: " + er.message);
                return null;
            }
        },
        /**
         * @returns {$.Oda.Controller.Tests}
         */
        testOffline: function () {
            try {
                params = {
                    key: $.Oda.Context.rest+"vendor/atbq/oda/resources/api/sys/offline",
                    attrs: {},
                    datas: {strErreur: "", data: {}, statut: 5, id_transaction: 0, metro: Object}
                };

                var d = new Date();
                var date = d.getTime() - 2000;
                params.recordDate = date;

                for (var indice in $.Oda.Cache.cache) {
                    var elt = $.Oda.Cache.cache[indice];
                    if ((elt.key === params.key) && ($.Oda.Tooling.deepEqual(elt.attrs, params.attrs))) {
                        $.Oda.Cache.cache.splice(indice, 1);
                    }
                }

                $.Oda.Cache.cache.push(params);
                $.Oda.Storage.set("ODA-CACHE-" + $.Oda.Session.code_user, $.Oda.Cache.cache);

                $.Oda.Interface.callRest($.Oda.Context.rest+"vendor/atbq/oda/resources/api/sys/offline", {callback: function(response){
                    console.log(response);
                }});
                return this;
            } catch (er) {
                $.Oda.Log.error("$.Oda.Controller.Tests.testOffline: " + er.message);
                return null;
            }
        },
    };

}());