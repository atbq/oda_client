(function () {
    'use strict';

    $.Oda.Controller.Support = {
        /**
         * @returns {$.Oda.Controller.Support}
         */
        start: function () {
            try {
                return this;
            } catch (er) {
                $.Oda.Log.error("$.Oda.Controller.Support.start: " + er.message);
                return null;
            }
        }
    };

}());