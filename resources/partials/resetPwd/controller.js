(function () {
    'use strict';

    $.Oda.Controller.ResetPwd = {
        /* $.Oda.Controller.ResetPwd.userCode */
        userCode: null,
        /**
         * @returns {$.Oda.Controller.ResetPwd}
         */
        start: function () {
            try {
                var params = $.Oda.Tooling.getParameterGet({url: $.Oda.Context.window.location});
                var data = $.Oda.Tooling.odaCryptToString(params.token);
                data = JSON.parse(data);
                var now = new Date();
                now = now.getTime();
                if(!data.hasOwnProperty("userCode") || !data.hasOwnProperty("valideDate")){
                    $.Oda.Log.error("Token for reset password not valid");
                    $.Oda.Router.navigateTo({route: "404"});
                }else if((data.userCode !== undefined) && (data.userCode !== "") && (data.valideDate !== undefined) && (data.valideDate > now)){
                    $('#userCode').html(data.userCode);
                    $.Oda.Controller.ResetPwd.userCode = data.userCode;
                    $.Oda.Scope.Gardian.add({
                        id: "resetPwd",
                        listElt: ["email","pwd", "pwd2"],
                        function: function(params){
                            if( ($("#email").data("isOk")) && ($("#pwd").data("isOk")) && ($("#pwd2").data("isOk")) && ($("#pwd").val() === $("#pwd2").val()) ){
                                $("#submit").btEnable();
                            }else{
                                $("#submit").btDisable();
                            }
                        }
                    });
                }else{
                    $.Oda.Log.error("Token params for reset password not valid");
                    $.Oda.Router.routes["404"].go();
                }
                return this;
            } catch (er) {
                $.Oda.Log.error("$.Oda.Controller.ResetPwd.start: " + er.message);
                return null;
            }
        },
        /**
         * @returns {$.Oda.Controller.ResetPwd}
         */
        reset: function () {
            try {
                $.Oda.Interface.callRest($.Oda.Context.rest + "vendor/atbq/oda/resources/api/user/pwd/", {
                    crypt: true,
                    type: "put", 
                    callback: function(response){
                        $.Oda.Router.navigateTo({route: "home"});
                    }
                }, {
                    userCode: $.Oda.Controller.ResetPwd.userCode,
                    pwd: $("#pwd").val(),
                    email: $("#email").val()
                });
                return this;
            } catch (er) {
                $.Oda.Log.error("$.Oda.Controller.ResetPwd.reset: " + er.message);
                return null;
            }
        }
    };

}());