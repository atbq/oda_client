(function () {
    'use strict';

    $.Oda.Controller.Forgot = {
        /**
         * @returns {$.Oda.Controller.Forgot}
         */
        start: function() {
            try {
                $.Oda.Scope.Gardian.add({
                    id: "gardianForgot",
                    listElt: ["code", "mail"],
                    function: function(params){
                        if( ($("#code").data("isOk")) && ($("#mail").data("isOk")) ){
                            $("#submit").btEnable();
                        }else{
                            $("#submit").btDisable();
                        }
                    }
                });
                return this;
            } catch (er) {
                $.Oda.Log.error("$.Oda.Controller.Forgot.start: " + er.message);
                return null;
            }
        },
        /**
         * @returns {$.Oda.Controller.Forgot}
         */
        getAccount: function() {
            try {
                var $submit = $("#submit");
                var $code = $("#code");
                var $mail = $("#mail");
                $submit.btDisable();
                $.Oda.Interface.callRest($.Oda.Context.rest+"vendor/atbq/oda/resources/api/user/search/", {
                    callback: function(results){
                        if(!results.data){
                            $.Oda.Display.Notification.warningI8n("oda-forgot.unknown");
                            $code.val("");
                            $mail.val("");
                            $submit.btEnable();
                        }else{
                            $.Oda.Interface.callRest($.Oda.Context.rest + "vendor/atbq/oda/resources/api/session/resetPwd", {
                                callback: function(results){
                                    if(results.strErreur === ""){
                                        $.Oda.Display.Notification.successI8n("oda-forgot.mailSend");
                                    }else{
                                        $.Oda.Display.Notification.errorI8n("oda-forgot.mailFail");
                                    }
                                    $code.val("");
                                    $mail.val("");
                                    $submit.btEnable();
                                }
                            },
                            {
                                code: $code.val(),
                                mail: $mail.val()
                            });
                        }
                    }
                }, {
                    code: $code.val(),
                    mail: $mail.val()
                });
                return this;
            } catch (er) {
                $.Oda.Log.error("$.Oda.Controller.Forgot.getAccount: " + er.message);
                return null;
            }
        }
    };

}());