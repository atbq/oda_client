(function () {
    'use strict';

    $.Oda.Controller.Subscrib = {
        /**
         * @returns {$.Oda.Controller.Subscrib}
         */
        start: function() {
            try {
                $.Oda.Scope.Gardian.add({
                    id: "gardianSubscrib",
                    listElt: ["firstName", "lastName", "mail", "password"],
                    function: function(params){
                        var login;
                        if(($("#firstName").data("isOk")) && ($("#lastName").data("isOk")) ){
                            var first = $("#firstName").val().replace(/[^\w\s]/gi, '');
                            var last = $("#lastName").val().replace(/[^\w\s]/gi, '');

                            login = (first.substring(0,2) + last.substring(0,2)).toUpperCase();
                            $("#login").text(login);
                        }else{
                            $("#login").text("...");
                        }
                        if(($("#firstName").data("isOk")) && ($("#lastName").data("isOk")) && ($("#mail").data("isOk")) && ($("#password").data("isOk"))){
                            $("#submit").btEnable();
                        }else{
                            $("#submit").btDisable();
                        }
                    }
                });
                return this;
            } catch (er) {
                $.Oda.Log.error("$.Oda.Controller.Subscrib.start: " + er.message);
                return null;
            }
        },
        /**
         * @returns {$.Oda.Controller.Subscrib}
         */
        createAccount: function() {
            try {
                $("#submit").btDisable();
                $.Oda.Interface.callRest($.Oda.Context.rest + "vendor/atbq/oda/resources/api/user/", {
                    crypt: true,
                    type: "post", 
                    callback: function(response){
                        var message_html  = $.Oda.Display.TemplateHtml.create({
                            template: "mailSubscribe",
                            scope: {
                                code_user: response.data.userCode,
                                firstName: $("#firstName").val(),
                                lastName: $("#lastName").val(),
                                siteUrl: $.Oda.Context.host
                            }
                        });

                        var sujet = "[ODA-" + $.Oda.Interface.getParameter("instance_name") + "]Création de compte.";

                        var contact_mail_administrateur = $.Oda.Interface.getParameter("contact_mail_administrateur");
                        
                        var msgSuccess = "F&eacute;licitation votre compte a bien &eacute;t&eacute; cr&eacute;&eacute; (N&deg;"+response.data.id+", identifiant : "+response.data.userCode+").";
                                    
                        $.Oda.Interface.mail({
                            to: response.data.mail,
                            from: contact_mail_administrateur,
                            fromLabel: "Service Mail ODA",
                            reply: contact_mail_administrateur,
                            replyLabel: "Service Mail ODA",
                            cc: contact_mail_administrateur,
                            bodyHtml: message_html,
                            subject: sujet,
                            callback: function(response){
                                if(response.data){
                                    $.Oda.Display.Notification.info(msgSuccess);
                                }else{
                                    $.Oda.Display.Notification.error("fail");
                                }
                                $("#firstName").val("");
                                $("#lastName").val("");
                                $("#mail").val("");
                                $("#password").val("");
                                $("#login").text("");
                                
                                $("#submit").btDisable();
                            }
                        });
                    }
                }, { 
                    lastName: $("#lastName").val(), 
                    firstName: $("#firstName").val(), 
                    mail: $("#mail").val(), 
                    password: $("#password").val(), 
                    userCode: $("#login").text() 
                });
                return this;
            } catch (er) {
                $.Oda.Log.error("$.Oda.Controller.Subscrib.createAccount: " + er.message);
                return null;
            }
        },
    };

}());