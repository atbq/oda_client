(function () {
    'use strict';

    $.Oda.Controller.Navigation = {
        ranks: [],
        pages: [],
        rights: [],
        currentRankId: null,
        /**
         * @returns {$.Oda.Controller.Navigation}
         */
        start: function() {
            try {
                $.Oda.Interface.callRest($.Oda.Context.rest+"vendor/atbq/oda/resources/api/navigation/rank/", {callback: function(response){
                    $.Oda.Controller.Navigation.ranks = response.data.data;
                    $.Oda.Interface.callRest($.Oda.Context.rest+"vendor/atbq/oda/resources/api/navigation/page/", {callback: function(response){
                        for(var index in response.data.data){
                            var elt = response.data.data[index];
                            if (!$.Oda.Tooling.isInArray(elt.route, $.Oda.Router.routesAllowedDefault)) {
                                $.Oda.Controller.Navigation.pages.push(elt);
                            }
                        }
                        $.Oda.Interface.callRest($.Oda.Context.rest+"vendor/atbq/oda/resources/api/navigation/rights/", {callback: function(response){
                            $.Oda.Controller.Navigation.rights = $.Oda.Controller.Navigation.transformRights(response.data.data);
                            $.Oda.Controller.Navigation.displayMapping();
                        }});
                    }});
                }});
                return this;
            } catch (er) {
                $.Oda.Log.error("$.Oda.Controller.Navigation.start: " + er.message);
                return null;
            }
        },
        /**
         * @returns {$.Oda.Controller.Navigation}
         */
        transformRights: function(datas) {
            try {
                var rights = {};
                for(var index in datas){
                    var right = datas[index];
                    var listPageRec = right.menu_ids.split(';');
                    var listPage = {};
                    for(var indexPage in $.Oda.Controller.Navigation.pages){
                        var page = $.Oda.Controller.Navigation.pages[indexPage];
                        listPage[page.id] = $.Oda.Tooling.isInArray(page.id, listPageRec);
                    }
                    rights[right.rank_id] = listPage;
                }
                return rights;
            } catch (er) {
                $.Oda.Log.error("$.Oda.Controller.Navigation.transformRights: " + er.message);
                return null;
            }
        },
        /**
         * @returns {$.Oda.Controller.Navigation}
         */
        displayMapping: function() {
            try {
                var $rankDiv = $('#rankdiv');
                $rankDiv.html('');
                for(var index in $.Oda.Controller.Navigation.ranks){
                    var rank = $.Oda.Controller.Navigation.ranks[index];
                    if(rank.index !== "1"){
                        var strHtml = $.Oda.Display.TemplateHtml.create({
                            template: "rank-tpl",
                            scope:{
                                label: rank.label,
                                index: rank.index
                            }
                        });
                        $rankDiv.append(strHtml);
                        $.Oda.Controller.Navigation.currentRankId = rank.id;
                        $.Oda.Display.Table.createDataTable({
                            target: "mapping_"+rank.index+"_div",
                            data: $.Oda.Controller.Navigation.pages,
                            option: {
                                aaSorting: [1, 'asc'],
                                pageLength: 50,
                                lengthChange: false,
                                searching: false,
                                paging: false,
                                info: false
                            },
                            attribute: [
                                {
                                    header: '<oda-label oda-label-value="oda-navigation.id"/>',
                                    size: "50px",
                                    align: "center",
                                    value: function(data, type, full, meta, row){
                                        return row.id;
                                    }
                                },
                                {
                                    header: '<oda-label oda-label-value="oda-navigation.label"/>',
                                    value: function(data, type, full, meta, row){
                                        return $.Oda.I8n.getByString(row.label);
                                    }
                                },
                                {
                                    header: '<oda-label oda-label-value="oda-navigation.select"/>',
                                    value: function(data, type, full, meta, row){
                                        var checkId = 'check_'+$.Oda.Controller.Navigation.currentRankId+'_'+row.id;
                                        return '<div id="tmp_'+checkId+'"/>';
                                    }
                                }
                            ]
                        });
                        $('[id^="tmp_"]').each(function(index){
                            var $elt = $(this);
                            var id = $elt.attr('id');
                            var checkId = id.substr(4);
                            var rankId = checkId.split('_')[1];
                            var pageId = checkId.split('_')[2];
                            $elt.html('<oda-input-checkbox oda-input-checkbox-name="'+checkId+'" oda-input-checkbox-value="'+(($.Oda.Controller.Navigation.rights[rankId][pageId])?'true':'false')+'" />');
                            $('#'+checkId).on('change',function(e){
                                $.Oda.Controller.Navigation.updateRight({checkId: checkId});
                            });
                        });
                    }
                }
                return this;
            } catch (er) {
                $.Oda.Log.error("$.Oda.Controller.Navigation.displayMapping: " + er.message);
                return null;
            }
        },
        /**
         * @returns {$.Oda.Controller.Navigation}
         */
        updateRight: function(p) {
            try {
                var rec = ";";
                var rankId =  p.checkId.split("_")[1];
                var strSearch = "check_" + rankId;
                $('[id^="'+strSearch+'"]').each(function(index){
                    var $elt = $(this);
                    var id = $elt.attr('id');
                    var pageId = id.split('_')[2];
                    if($elt.val() === "true"){
                        rec += pageId + ";"
                    }
                })
                $.Oda.Interface.callRest($.Oda.Context.rest+"vendor/atbq/oda/resources/api/navigation/right/"+rankId, {type: "PUT", callback: function(response){
                    $.Oda.Display.Notification.successI8n('oda-navigation.updateSuccess');
                }},{
                    value: rec
                });
                return this;
            } catch (er) {
                $.Oda.Log.error("$.Oda.Controller.Navigation.updateRight: " + er.message);
                return null;
            }
        }
    };

}());