# Change Log

## [1.1.77] - 2020-01-29

* fix(oda-input-select): insert in html mode

## [1.1.76] - 2020-01-27

* fix(regex): Add quote in noinjection

## [1.1.75] - 2019-12-02

* fix(Display): $.Oda.Display.TemplateHtml.create resilient

## [1.1.74] - 2019-11-15

* fix(Tooling): Tooling.Json.toDOM catch simple quote from WYSIWYG

## [1.1.73] - 2019-11-14

* fix(Tooling): Tooling.Json.toDOM remplacement simple quote by htlm entities 

## [1.1.72] - 2019-11-06

* fix(Tooling): Tooling.Json.toDOM improve remplacement of simple quote

## [1.1.71] - 2019-10-30

* fix(regex): $.Oda.Regexs.noInjection bloque les dates

## [1.1.70] - 2019-10-14

* refacto(regex): noInjection stronger

## [1.1.69] - 2019-10-14

* refacto(build): refacto

## [1.1.68] - 2019-10-11

* feat(build): package config in prod mode, close #37

## [1.1.67] - 2019-10‑01

* fix(resetPwd):calc now

## [1.1.66] - 2019-09‑27

* fix(resetPwd):calc now

## [1.1.65] - 2019-09-26

* fix(call): close #15

## [1.1.64] - 2019-09-16

* feat(Security): add $.Oda.Security.consoleLock

## [1.1.63] - 2019-09-10

* feat(Tooling): add $.Oda.Tooling.stripHtml

## [1.1.62] - 2019-09-10

* fix(loadPartial): check exist $.Oda.App.Controller

## [1.1.61] - 2019-09-09

* fix(forgot): check callback mail

## [1.1.60] - 2019-09-09

* fix(forgot): check callback for mail, ses

## [1.1.59] - 2019-09-08

* feat(forgot): add backend for send mail

## [1.1.58] - 2019-08-29

* feat(resetPwd, subscrib): crypt output (pwd)

## [1.1.57] - 2019-08-26

* feat(interface): add crypt option

## [1.1.56] - 2019-08-08

* feat(Router):close #33 add dispose for route
* fix(deploy):close #28
* feat(datatables):add total

## [1.1.55] - 2019-08-06

* fix(TemplateHtml):remove catch for eval() trans error to create()

## [1.1.54] - 2019-07-03

* fix call google auth if not setup

## [1.1.53] - 2019-06-14

* add $.fn.reset for form

## [1.1.52] - 2019-06-07

* $.Oda.Tooling.detectBrower: add Mozilla impact $.Oda.Date.dateFormat

## [1.1.51] - 2019-05-23

* $.Oda.Date.dateFormat: fix safari, close #34

## [1.1.50] - 2019-04-30

* Add method $.Oda.Display.Table.destroyAll
* Add method $.Oda.REST, ALPHA !

## [1.1.49] - 2019-04-11

* test slidemenu exist before remove for logout

## [1.1.48] - 2019-04-11

* remove notification error for prod

## [1.1.47] - 2019-03-08

* go to warning for $.Oda.Display.TemplateHtml

## [1.1.46] - 2019-03-06

* optim menuSlide and click out for toggle

## [1.1.45] - 2019-02-26

* reset hash on logout

## [1.1.44] - 2019-02-04

* Refacto partial admin

## [1.1.43] - 2019-02-01

* add $.Oda.I8n.defaultLanguage

## [1.1.42] - 2019-01-31

* Update navigation page

## [1.1.41] - 2019-01-30

* Fix label for auth

## [1.1.40] - 2019-01-27

* auth send in uppercase login

## [1.1.39] - 2019-01-28

* Remove url site on forgot mail

* Add feature for patch (change all num), but it don't work because G.J. is a noob

## [1.1.38] - 2019-01-18

* Add new format for entry date of $.Oda.Date.dateFormat for manage GMT
* Add feature $.Oda.Date.dateTimeLocaleToIso

## [1.1.37] - 2019-01-16

* Add template name in error of $.Oda.Display.TemplateHtml.create

## [1.1.36] - 2019-01-10

* Add ttl in parameter of $.Oda.Security.auth