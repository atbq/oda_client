var expect = require('chai').expect;
var assert = require('assert');
var should = require('chai').should();
global.$ = require('./../dist/Oda.js');
console.log('Oda version: ', $.Oda.version);

describe("$.Oda.Display", function() {
    describe("$.Oda.Display.TemplateHtml", function() {

        describe("$.Oda.Display.TemplateHtml.eval", function() {
            it("Test 1 user", function(){
                var recieve = $.Oda.Display.TemplateHtml.eval({
                    variable: 'user',
                    scope: {
                        user: 'fabrice'
                    }
                });
                var waiting = 'fabrice';
                assert.equal(recieve, waiting);
            });

            it("Test 2 a+b", function(){
                var recieve = $.Oda.Display.TemplateHtml.eval({
                    variable: 'a + b',
                    scope: {
                        a: 1,
                        b: 2
                    }
                });
                var waiting = 3;
                assert.equal(recieve, waiting);
            });
        });

        describe("$.Oda.Display.TemplateHtml.scope", function() {
            it("Test 1 ok", function(){
                var recieve = $.Oda.Display.TemplateHtml.scope({
                    source: 'Coucou {{user}} !!',
                    scope: {
                        user: 'fabrice'
                    }
                });
                var waiting = 'Coucou fabrice !!';
                assert.equal(recieve, waiting);
            });

            it("Test 2 no scope", function(){
                var recieve = $.Oda.Display.TemplateHtml.scope({
                    source: 'Coucou {{user}} !!'
                });
                var waiting = 'Coucou {{user}} !!';
                assert.equal(recieve, waiting);
            });

            it("Test 3 no variabble", function(){
                var recieve = $.Oda.Display.TemplateHtml.scope({
                    source: 'Coucou fabrice !!',
                    scope: {
                        user: 'fabrice'
                    }
                });
                var waiting = 'Coucou fabrice !!';
                assert.equal(recieve, waiting);
            });

            it("Test 4 no match scope", function(){
                var recieve = $.Oda.Display.TemplateHtml.scope({
                    source: 'Coucou {{user}} !!',
                    scope: {
                        him: 'fabrice'
                    }
                });
                var waiting = 'Coucou null !!';
                assert.equal(recieve, waiting);
            });
        });
    });
});

describe("$.Oda.Tooling", function() {
    describe("$.Oda.Tooling.checkRegEx", function() {
        describe("mail", function() {
            it("Test 1 team@atbq.fr ok", function(){
                var recieve = $.Oda.Tooling.checkRegEx({
                    str: 'team@atbq.fr',
                    regex: 'Oda.Regexs:mail'
                });
                assert.equal(recieve, true);
            });

            it("Test 2 teamatbq.fr ko", function(){
                var recieve = $.Oda.Tooling.checkRegEx({
                    str: 'teamatbq.fr',
                    regex: 'Oda.Regexs:mail'
                });
                assert.equal(recieve, false);
            });
        });

        describe("login", function() {
            it("Test 1 FA9 ok", function(){
                var recieve = $.Oda.Tooling.checkRegEx({
                    str: 'FA9',
                    regex: 'Oda.Regexs:login'
                });
                assert.equal(recieve, true);
            });

            it("Test 2 FA ko", function(){
                var recieve = $.Oda.Tooling.checkRegEx({
                    str: 'FA',
                    regex: 'Oda.Regexs:login'
                });
                assert.equal(recieve, false);
            });

            it("Test 3 FARO+ ko", function(){
                var recieve = $.Oda.Tooling.checkRegEx({
                    str: 'FARO+',
                    regex: 'Oda.Regexs:login'
                });
                assert.equal(recieve, false);
            });
        });

        describe("pass", function() {
            it("Test 1 FAR1 ok", function(){
                var recieve = $.Oda.Tooling.checkRegEx({
                    str: 'FAR1',
                    regex: 'Oda.Regexs:pass'
                });
                assert.equal(recieve, true);
            });

            it("Test 2 FAR ko", function(){
                var recieve = $.Oda.Tooling.checkRegEx({
                    str: 'FAR',
                    regex: 'Oda.Regexs:pass'
                });
                assert.equal(recieve, false);
            });

            it("Test 3 FARO+ ko", function(){
                var recieve = $.Oda.Tooling.checkRegEx({
                    str: 'FARO+',
                    regex: 'Oda.Regexs:pass'
                });
                assert.equal(recieve, false);
            });
        });

        describe("firstName", function() {
            it("Test 1 fabrice ok", function(){
                var recieve = $.Oda.Tooling.checkRegEx({
                    str: 'fabrice',
                    regex: 'Oda.Regexs:firstName'
                });
                assert.equal(recieve, true);
            });

            it("Test 2 'jean loup' ok", function(){
                var recieve = $.Oda.Tooling.checkRegEx({
                    str: 'jean loup',
                    regex: 'Oda.Regexs:firstName'
                });
                assert.equal(recieve, true);
            });

            it("Test 3 'script:' ko", function(){
                var recieve = $.Oda.Tooling.checkRegEx({
                    str: 'script:',
                    regex: 'Oda.Regexs:firstName'
                });
                assert.equal(recieve, false);
            });

            it("Test 4 'test()' ko", function(){
                var recieve = $.Oda.Tooling.checkRegEx({
                    str: 'test()',
                    regex: 'Oda.Regexs:firstName'
                });
                assert.equal(recieve, false);
            });

            it("Test 5 quote ok", function(){
                var recieve = $.Oda.Tooling.checkRegEx({
                    str: 'L’apos',
                    regex: 'Oda.Regexs:firstName'
                });
                assert.equal(recieve, true);
            });
        });

        describe("lastName", function() {
            it("Test 1 fabrice ok", function(){
                var recieve = $.Oda.Tooling.checkRegEx({
                    str: 'fabrice',
                    regex: 'Oda.Regexs:lastName'
                });
                assert.equal(recieve, true);
            });

            it("Test 2 'jean loup' ok", function(){
                var recieve = $.Oda.Tooling.checkRegEx({
                    str: 'jean loup',
                    regex: 'Oda.Regexs:lastName'
                });
                assert.equal(recieve, true);
            });

            it("Test 3 'script:' ko", function(){
                var recieve = $.Oda.Tooling.checkRegEx({
                    str: 'script:',
                    regex: 'Oda.Regexs:lastName'
                });
                assert.equal(recieve, false);
            });

            it("Test 4 'test()' ko", function(){
                var recieve = $.Oda.Tooling.checkRegEx({
                    str: 'test()',
                    regex: 'Oda.Regexs:lastName'
                });
                assert.equal(recieve, false);
            });

            it("Test 5 quote ok", function(){
                var recieve = $.Oda.Tooling.checkRegEx({
                    str: 'L’apos',
                    regex: 'Oda.Regexs:lastName'
                });
                assert.equal(recieve, true);
            });
        });

        describe("noInjection", function() {
            it("Test 1 fabrice ok", function(){
                var recieve = $.Oda.Tooling.checkRegEx({
                    str: 'fabrice',
                    regex: 'Oda.Regexs:noInjection'
                });
                assert.equal(recieve, true);
            });

            it("Test 2 '2031-02-05 12:15' ok", function(){
                var recieve = $.Oda.Tooling.checkRegEx({
                    str: '2031-02-05 12:15',
                    regex: 'Oda.Regexs:noInjection'
                });
                assert.equal(recieve, true);
            });

            it("Test 3 '06.535.24 salut t'es la, à moi.' ok", function(){
                var recieve = $.Oda.Tooling.checkRegEx({
                    str: "06.535.24 salut t'es la, à moi.",
                    regex: 'Oda.Regexs:noInjection'
                });
                assert.equal(recieve, true);
            });

            it("Test 4 '<script>alert()</scrip>", function(){
                var recieve = $.Oda.Tooling.checkRegEx({
                    str: '<script>alert()</scrip>',
                    regex: 'Oda.Regexs:noInjection'
                });
                assert.equal(recieve, false);
            });

            it("Test 5 'apo et perc'", function(){
                var recieve = $.Oda.Tooling.checkRegEx({
                    str: 'l’apo et 50% perc',
                    regex: 'Oda.Regexs:noInjection'
                });
                assert.equal(recieve, true);
            });
        });

        describe("phone", function() {
            it("Test 1 '0123456789' ok", function(){
                var recieve = $.Oda.Tooling.checkRegEx({
                    str: '0123456789',
                    regex: 'Oda.Regexs:phone'
                });
                assert.equal(recieve, true);
            });

            it("Test 2 '01.23.45.67.89' ok", function(){
                var recieve = $.Oda.Tooling.checkRegEx({
                    str: '01.23.45.67.89',
                    regex: 'Oda.Regexs:phone'
                });
                assert.equal(recieve, true);
            });

            it("Test 3 '01 23 45 67 89' ok", function(){
                var recieve = $.Oda.Tooling.checkRegEx({
                    str: "01 23 45 67 89",
                    regex: 'Oda.Regexs:phone'
                });
                assert.equal(recieve, true);
            });

            it("Test 4 autre chose 001", function(){
                var recieve = $.Oda.Tooling.checkRegEx({
                    str: 'autre chose 001',
                    regex: 'Oda.Regexs:phone'
                });
                assert.equal(recieve, false);
            });
        });
    });

    describe("$.Oda.Tooling.Json", function() {
        describe("toDOM", function() {
            it("Test 1 normal", function(){
                var recieve = $.Oda.Tooling.Json.toDOM({
                    json: [{"coucou":"value"}]
                });
                assert.equal(recieve, "[{'coucou':'value'}]");
            });

            it("Test 2 simple quote", function(){
                var recieve = $.Oda.Tooling.Json.toDOM({
                    json: {title: "Test d'évènement"}
                });
                assert.equal(recieve, "{'title':'Test d&rsquo;évènement'}");
            });

            it("Test 3 simple quote unicode", function(){
                var recieve = $.Oda.Tooling.Json.toDOM({
                    json: {title: "Test d&#39;évènement"}
                });
                assert.equal(recieve, "{'title':'Test d&rsquo;évènement'}");
            });
        });

        describe("fromDOM", function() {
            it("Test 1 normal ok", function(){
                var recieve = $.Oda.Tooling.Json.fromDOM({str:"{'id':'8','title':'sdfsdfsdf','start':'2017-12-14 15:36:00','status':{'id':3,'label':'event.consume'}}"});
                var waiting = {
                    id: "8",
                    title: "sdfsdfsdf",
                    start: "2017-12-14 15:36:00",
                    status: {
                        id: 3,
                        label: "event.consume"
                    }
                }
                assert.deepEqual(recieve, waiting);
            });
        });
    });

    describe("$.Oda.Tooling.stringToOdaCrypt", function() {
        it("Test 1 normal", function(){
            var recieve = $.Oda.Tooling.stringToOdaCrypt('{"userCode":"ILRO","valideDate":1491902766000}');
            assert.equal(recieve, "7b2275736572436f6465223a22494c524f222c2276616c69646544617465223a313439313930323736363030307d");
        });

        it("Test 2 quote from WYSIWYG", function(){
            var recieve = $.Oda.Tooling.stringToOdaCrypt("{'title':'C&rsquo;est le test de l&rsquo;apostrophe', 'comment':'C&#39;est le test de l&#39;apostrophe\\n'}");
            assert.equal(recieve, "7b277469746c65273a274326727371756f3b657374206c652074657374206465206c26727371756f3b61706f7374726f706865272c2027636f6d6d656e74273a2743262333393b657374206c652074657374206465206c262333393b61706f7374726f7068655c6e277d");
        });
    });

    describe("$.Oda.Tooling.odaCryptToString", function() {
        it("Test 1 normal", function(){
            var recieve = $.Oda.Tooling.odaCryptToString('7b2275736572436f6465223a22494c524f222c2276616c69646544617465223a313439313930323736363030307d');
            assert.equal(recieve, '{"userCode":"ILRO","valideDate":1491902766000}');
        });

        it("Test 2 quote from WYSIWYG", function(){
            var recieve = $.Oda.Tooling.odaCryptToString('7b277469746c65273a274326727371756f3b657374206c652074657374206465206c26727371756f3b61706f7374726f706865272c2027636f6d6d656e74273a2743262333393b657374206c652074657374206465206c262333393b61706f7374726f7068655c6e277d');
            assert.equal(recieve, "{'title':'C&rsquo;est le test de l&rsquo;apostrophe', 'comment':'C&#39;est le test de l&#39;apostrophe\\n'}");
        });
    });

    describe("$.Oda.Tooling.findBetweenWords", function() {
        it("Test 1 found", function(){
            var receive = $.Oda.Tooling.findBetweenWords({
                str: 'Hello here you',
                first: 'Hello',
                last: 'you'
            });
            assert.deepEqual(receive, [' here ']);
        });

        it("Test 2 not found", function(){
            var receive = $.Oda.Tooling.findBetweenWords({
                str: 'Hello here you',
                first: 'Truc',
                last: 'you'
            });
            assert.deepEqual(receive, []);
        });

        it("Test 3 found {", function(){
            var receive = $.Oda.Tooling.findBetweenWords({
                str: 'Hello {{here}} you',
                first: '{{',
                last: '}}'
            });
            assert.deepEqual(receive, ['here']);
        });

        it("Test 4 undefined source", function(){
            var receive = $.Oda.Tooling.findBetweenWords({
                str: undefined,
                first: '{{',
                last: '}}'
            });
            assert.deepEqual(receive, []);
        });

        it("Test 5 null source", function(){
            var receive = $.Oda.Tooling.findBetweenWords({
                str: null,
                first: '{{',
                last: '}}'
            });
            assert.deepEqual(receive, []);
        });

        it("Test 6 empty source", function(){
            var receive = $.Oda.Tooling.findBetweenWords({
                str: '',
                first: '{{',
                last: '}}'
            });
            assert.deepEqual(receive, []);
        });

        it("Test 7 too small source", function(){
            var receive = $.Oda.Tooling.findBetweenWords({
                str: '123',
                first: '{{',
                last: '}}'
            });
            assert.deepEqual(receive, []);
        });
    });
});