QUnit.module( "Date" );

QUnit.test( "$.Oda.Date.dateFormat", function(assert) {
    assert.equal($.Oda.Date.dateFormat(new Date('2017-01-12'), 'YYYY-MM-DD'), "2017-01-12", "Test 1" );
    assert.equal($.Oda.Date.dateFormat('2017-01-12', 'YYYY-MM-DD'), "2017-01-12", "Test 2" );
    assert.equal($.Oda.Date.dateFormat('2017-01-12 12:50:41 GMT', 'YYYY-MM-DD'), "2017-01-12", "Test 3" );
    assert.equal($.Oda.Date.dateFormat('2017-01-12 12:50:43 GMT+1', 'YYYY-MM-DD'), "2017-01-12", "Test 4" );
    assert.equal($.Oda.Date.dateFormat('2017-01-12 12:50:44 GMT-12', 'YYYY-MM-DD'), "2017-01-13", "Test 5" );
    assert.equal($.Oda.Date.dateFormat('2017-01-12 12:50:45', 'YYYY-MM-DD'), "2017-01-12", "Test 6" );
});