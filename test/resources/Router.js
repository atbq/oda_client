QUnit.module("Router");

QUnit.test("$.Oda.Router.workerMiddleWare with callbacks", function (assert) {
    //Middlewares
    function middleWare1(p) {
        assert.step('1');
        setTimeout(function(){
            p.callback();
        }, 100);
    }
    function middleWare2(p) {
        assert.step('2');
        p.callback();
    }
    function middleWare3(p) {
        assert.step('3');
        p.callback();
    }
    function middleWare4(p) {
        assert.step('4');
        p.callback();
    }

    //Assign middlewares in ODA
    $.Oda.Router.MiddleWares.middleWare1 = middleWare1;
    $.Oda.Router.MiddleWares.middleWare2 = middleWare2;
    $.Oda.Router.MiddleWares.middleWare3 = middleWare3;
    $.Oda.Router.MiddleWares.middleWare4 = middleWare4;

    //Middlewares queue
    $.Oda.Router.middlewareQueue = [middleWare1, middleWare2, middleWare3, middleWare4];

    //Run test
    var thenable = new Promise(function (resolve) {
        var save = $.Oda.Router.finishGo;
        $.Oda.Router.finishGo = function () {
            $.Oda.Router.finishGo = save;
            assert.verifySteps(['1', '2', '3', '4'], 'Steps are OK');
            resolve();
        };
        $.Oda.Router.workerMiddleWare();
    });
    return thenable;
});

QUnit.test("$.Oda.Router.workerMiddleWare no callbacks", function (assert) {
    //Map success test
    var success = {
        '1': false,
        '2': false,
        '3': false,
        '4': false
    };

    //Middlewares
    function middleWare1() {
        setTimeout(function () {
            success['1'] = true;
        }, 100);
    }
    function middleWare2() {
        setTimeout(function () {
            success['2'] = true;
        }, 400);
    }
    function middleWare3() {
        setTimeout(function () {
            success['3'] = true;
        }, 150);
    }
    function middleWare4() {
        setTimeout(function () {
            success['4'] = true;
        }, 200);
    }

    //Assign middlewares in ODA
    $.Oda.Router.MiddleWares.middleWare1 = middleWare1;
    $.Oda.Router.MiddleWares.middleWare2 = middleWare2;
    $.Oda.Router.MiddleWares.middleWare3 = middleWare3;
    $.Oda.Router.MiddleWares.middleWare4 = middleWare4;

    //Middlewares queue
    $.Oda.Router.middlewareQueue = [middleWare1, middleWare2, middleWare3, middleWare4];

    //Run test
    var thenable = new Promise(function (resolve) {
        var save = $.Oda.Router.finishGo;
        $.Oda.Router.finishGo = function () {
            $.Oda.Router.finishGo = save;
            setTimeout(function () {
                assert.deepEqual(success, {
                    '1': true,
                    '2': true,
                    '3': true,
                    '4': true
                }, 'With no callbacks OK');
                resolve();
            }, 500);
        };
        $.Oda.Router.workerMiddleWare();
    });
    return thenable;
});

QUnit.test("$.Oda.Router.workerMiddleWare no callbacks and callbacks", function (assert) {
    //Map success test
    var success = {
        '1': false,
        '3': false
    };
    //Middlewares
    function middleWare1() {
        setTimeout(function () {
            success['1'] = true;
        }, 100);
    }
    function middleWare2(p) {
        assert.step('2');
        p.callback();
    }
    function middleWare3() {
        setTimeout(function () {
            success['3'] = true;
        }, 100);
    }
    function middleWare4(p) {
        assert.step('4');
        p.callback();
    }

    //Assign middlewares in ODA
    $.Oda.Router.MiddleWares.middleWare1 = middleWare1;
    $.Oda.Router.MiddleWares.middleWare2 = middleWare2;
    $.Oda.Router.MiddleWares.middleWare3 = middleWare3;
    $.Oda.Router.MiddleWares.middleWare4 = middleWare4;

    //Middlewares queue
    $.Oda.Router.middlewareQueue = [middleWare1, middleWare2, middleWare3, middleWare4];

    //Run test
    var thenable = new Promise(function (resolve) {
        var save = $.Oda.Router.finishGo;
        $.Oda.Router.finishGo = function () {
            $.Oda.Router.finishGo = save;
            assert.verifySteps(['2', '4'], 'Steps are OK');
            setTimeout(function () {
                assert.deepEqual(success, {
                    '1': true,
                    '3': true
                }, 'With no callbacks OK');
                resolve();
            }, 200);
        };
        $.Oda.Router.workerMiddleWare();
    });
    return thenable;
});