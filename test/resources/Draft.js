QUnit.module( "Draft" );

QUnit.test( "$.Oda.Tooling.stripHtml", function(assert) {
    assert.deepEqual($.Oda.Tooling.stripHtml({
        html: '<br>coucou<br>hello'
    }), ' coucou hello', "Test OK : Passed!" );

    assert.deepEqual($.Oda.Tooling.stripHtml({
        html: '<br>coucou<br>hello',
        by: 'a'
    }), 'acoucouahello', "Test OK : Passed!" );
});