QUnit.module( "Url" );

var strUrl1 = "http://localhost/how/#page?attr=1";
var objUrl1 = {
    protocol: "http",
    domain: ["localhost"],
    port: null,
    path: ["how"],
    pathEnd: "/",
    search: [],
    hash: "page",
    hashSearch: [
        {
            attr: "1"
        }
    ]
};

var strUrl2 = "https://test1.test2.localhost.com:8080/path1/path2/page1?arg1=a&arg2=2#page2?attr3=3&attr2=hello";
var objUrl2 = {
    protocol: "https",
    domain: ["test1","test2","localhost","com"],
    port: "8080",
    path: ["path1","path2", "page1"],
    pathEnd: null,
    search: [
        {
            arg1: "a"
        },
        {
            arg2: "2"
        }
    ],
    hash: "page2",
    hashSearch: [
        {
            attr3: "3"
        },
        {
            attr2: "hello"
        }
    ]
};

var strUrl3 = "http://localhost/how/#?attr=1";
var objUrl3 = {
    protocol: "http",
    domain: ["localhost"],
    port: null,
    path: ["how"],
    pathEnd: "/",
    search: [],
    hash: "",
    hashSearch: [
        {
            attr: "1"
        }
    ]
};

var strUrl4 = "http://localhost.com";
var objUrl4 = {
    protocol: "http",
    domain: ["localhost", "com"],
    port: null,
    path: [],
    pathEnd: null,
    search: [],
    hash: null,
    hashSearch: []
};

QUnit.test( "$.Oda.Tooling.Url.analyse", function(assert) {

    assert.deepEqual($.Oda.Tooling.Url.analyse(strUrl1), objUrl1, "Test OK : Passed!" );

    assert.deepEqual($.Oda.Tooling.Url.analyse(strUrl2), objUrl2, "Test OK : Passed!" );

    assert.deepEqual($.Oda.Tooling.Url.analyse(strUrl3), objUrl3, "Test OK : Passed!" );

    assert.deepEqual($.Oda.Tooling.Url.analyse(strUrl4), objUrl4, "Test OK : Passed!" );
});

QUnit.test( "$.Oda.Tooling.Url.buildUrl", function(assert) {

    assert.deepEqual($.Oda.Tooling.Url.buildUrl(objUrl1), strUrl1, "Test OK : Passed!" );

    assert.deepEqual($.Oda.Tooling.Url.buildUrl(objUrl2), strUrl2, "Test OK : Passed!" );

    assert.deepEqual($.Oda.Tooling.Url.buildUrl(objUrl3), strUrl3, "Test OK : Passed!" );

    assert.deepEqual($.Oda.Tooling.Url.buildUrl(objUrl4), strUrl4, "Test OK : Passed!" );
});

QUnit.test( "$.Oda.Tooling.Url.addAtt", function(assert) {

    assert.deepEqual($.Oda.Tooling.Url.addAtt({
        url: strUrl1,
        type: "hashSearch",
        attr: [{
            attr: "2"
        }]
    }), "http://localhost/how/#page?attr=2", "Test OK : Passed!" );

    assert.deepEqual($.Oda.Tooling.Url.addAtt({
        url: strUrl1,
        type: "hashSearch",
        attr: [{
            attr1: "2"
        },{
            attr2: "3"
        }]
    }), "http://localhost/how/#page?attr=1&attr1=2&attr2=3", "Test OK : Passed!" );
});

QUnit.test( "$.Oda.Tooling.Url.removeAtt", function(assert) {

    assert.deepEqual($.Oda.Tooling.Url.removeAtt({
        url: strUrl2,
        type: "hashSearch",
        attr: ["attr3","attr4"]
    }), "https://test1.test2.localhost.com:8080/path1/path2/page1?arg1=a&arg2=2#page2?attr2=hello", "Test OK : Passed!" );

    assert.deepEqual($.Oda.Tooling.Url.removeAtt({
        url: strUrl2,
        type: "search",
        attr: ["arg1","arg2"]
    }), "https://test1.test2.localhost.com:8080/path1/path2/page1#page2?attr3=3&attr2=hello", "Test OK : Passed!" );
});