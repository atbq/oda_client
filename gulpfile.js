var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();
plugins.merge = require('merge-stream');
plugins.tagVersion = require('gulp-tag-version');
plugins.filter = require('gulp-filter');
plugins.bump = require('gulp-bump');
plugins.minCss = require('gulp-minify-css');
plugins.uglify = require('gulp-uglify');
plugins.rename = require('gulp-rename');
plugins.concat = require('gulp-concat');

gulp.task('build', ['compress', 'scss'], function() {
});

gulp.task('compress', function() {
    gulp.src("dist/Oda.js")
        .pipe(plugins.uglify())
        .pipe(plugins.rename({
            extname: '.min.js'
        }))
        .pipe(gulp.dest('dist/'));
    return;
});

gulp.task('scss', function () {
    gulp.src('resources/css/**/*.scss')
        .pipe(plugins.concat('css.scss'))
        .pipe(plugins.sass().on('error', plugins.sass.logError))
        .pipe(plugins.autoprefixer({
            browsers: [
                "ie >= 9",
                "ie_mob >= 10",
                "ff >= 30",
                "chrome >= 34",
                "safari >= 7",
                "opera >= 23",
                "ios >= 7",
                "android >= 4.4",
                "bb >= 10"
            ]
        }))
        .pipe(plugins.minCss())
        .pipe(gulp.dest('resources/css/'));
});

gulp.task('watch',['scss'], function () {
    gulp.watch('css/**/*.scss' , ['scss']);
});

function inc(importance) {
    var packageVersion = gulp.src(['./package.json', './bower.json'])
        .pipe(plugins.bump({type: importance}))
        .pipe(gulp.dest('./', {overwrite: true}))
        .pipe(plugins.filter('bower.json'))
        .pipe(plugins.tagVersion());
    var odaVersion = gulp.src(['./dist/Oda.js'])
        .pipe(plugins.bump({type: importance}))
        .pipe(gulp.dest('./dist/'))
        .pipe(plugins.tagVersion('./dist/Oda.js'));
    return plugins.merge(packageVersion, odaVersion);
}

gulp.task('patch', function() { return inc('patch'); });
gulp.task('feature', function() { return inc('minor'); });
gulp.task('release', function() { return inc('major'); });