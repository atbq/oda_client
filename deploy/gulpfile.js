var gulp = require('gulp');
var replace = require('gulp-replace');

var OdaGulpConfig = {
    vendorName: "bower_components"
};

gulp.task('install-commun', [], function() {
    gulp.src(['commun/**/*', 'commun/.gitignore'])
        .pipe(gulp.dest('./../../../'));
    return;
});

//ex : gulp install-full --vendorName libs
gulp.task('install-full', ["install-commun"], function() {
    gulp.src(['full/index.html','full/gulpfile.js','full/package.json','full/jsTestDriver.conf'])
        .pipe(replace(/vendor/g, OdaGulpConfig.vendorName))
        .pipe(gulp.dest('./../../../'));
    gulp.src(['!full/index.html','!full/gulpfile.js','!full/package.json','!full/jsTestDriver.conf','full/**/*'])
        .pipe(gulp.dest('./../../../'));
    return;
});

//ex : gulp install-full --vendorName libs
gulp.task('install-app', ["install-commun"], function() {
    gulp.src(['app/index.html','app/gulpfile.js','app/package.json','app/jsTestDriver.conf'])
        .pipe(replace(/vendor/g, OdaGulpConfig.vendorName))
        .pipe(gulp.dest('./../../../'));
    gulp.src(['!app/index.html','!app/gulpfile.js','!app/package.json','!app/jsTestDriver.conf','app/**/*'])
        .pipe(gulp.dest('./../../../'));
    return;
});

//ex : gulp install-full --vendorName libs
gulp.task('install-mini', ["install-commun"], function() {
    gulp.src(['mini/index.html','mini/gulpfile.js','mini/package.json','mini/jsTestDriver.conf'])
        .pipe(replace(/vendor/g, OdaGulpConfig.vendorName))
        .pipe(gulp.dest('./../../../'));
    gulp.src(['!mini/index.html','!mini/gulpfile.js','!mini/package.json','!mini/jsTestDriver.conf','mini/**/*'])
        .pipe(gulp.dest('./../../../'));
    return;
});