var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();
plugins.minCss = require('gulp-minify-css');
plugins.uglify = require('gulp-uglify');
plugins.rename = require('gulp-rename');
plugins.sass = require("gulp-sass");
plugins.concat = require('gulp-concat');
plugins.browserSync = require('browser-sync');

gulp.task('build', ['compress', 'scss'], function() {
});

gulp.task('compress', function() {
    if(process.argv[4] === '-c' && process.argv[5] !== undefined){
        gulp.src(['config/config.'+process.argv[5]+'.js', 'js/OdaApp.js'])
        .pipe(plugins.concat('OdaApp.min.js'))
        .pipe(plugins.uglify({
            mangle: false
        }))
        .on('error', function (err) {
            gutil.log(gutil.colors.red('[Error]'), err.toString());
            process.exit(1);
        })
        .pipe(gulp.dest('js/'));
    }else{
        gulp.src("js/OdaApp.js")
        .pipe(plugins.uglify({
            mangle: false
        }))
        .on('error', function (err) {
            gutil.log(gutil.colors.red('[Error]'), err.toString());
            process.exit(1);
        })
        .pipe(plugins.rename({
            extname: '.min.js'
        }))
        .pipe(gulp.dest('js/'));
    }
    return;
});

gulp.task('browser-sync', function() {
    plugins.browserSync.init({
        proxy: "localhost:80/project/"
    });
    gulp.watch(["js/**/*","partials/**/*","i8n/**/*","css/**/*.css"], function(){
        plugins.browserSync.reload();
    });
});

gulp.task('scss', function () {
    gulp.src('css/**/*.scss')
        .pipe(plugins.sass().on('error', plugins.sass.logError))
        .pipe(plugins.autoprefixer({
            browsers: [
                "ie >= 9",
                "ie_mob >= 10",
                "ff >= 30",
                "chrome >= 34",
                "safari >= 7",
                "opera >= 23",
                "ios >= 7",
                "android >= 4.4",
                "bb >= 10"
            ]
        }))
        .pipe(plugins.minCss())
        .pipe(gulp.dest('css/'));
});

gulp.task('watch',['scss'], function () {
    gulp.watch('css/**/*.scss' , ['scss']);
});