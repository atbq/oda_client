(function () {
    'use strict';

    $.Oda.App.Controller.Home = {
        /**
         * @returns {$.Oda.App.Controller.Home}
         */
        start: function () {
            try {
                return this;
            } catch (er) {
                $.Oda.Log.error("$.Oda.App.Controller.Home.start: " + er.message);
                return null;
            }
        }
    }

}());